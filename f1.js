$( document ).ready(function() {
	$('#tit').fadeIn(1500);	

	$('#t1').on( 'click', '.del', function () {
		var $form=$(this).closest('tr');
    	$('#confirm').modal().on('click', '#delete', function() {
        	$form.remove();
        	$('#confirm').modal('hide');
        });
        $('#confirm').modal().on('click', '#cancel', function() {
    		$('#confirm').modal('hide');
		});
	});

	$(".add").click(function () {
		addRow('t1');
	});

	$('#t1').on( 'click', '.edit', function () {
		$(this).parent().parent().find('div').each (function() { 
  			$(this).replaceWith(function() {
   				return $('<input type="text" class="form-control" value="' + $(this).html() + '" />' );
			});
  		}); 
	});

	$('#t1').on( 'click', '.save', function () {
  		$(this).parent().parent().find('input').each (function() { 
  			$(this).replaceWith(function() {
   				return $('<div>' + $(this).val() + '</div>');
			});
  		});  		
  	});
});



function addRow(TableID) {
	var table = document.getElementById(TableID);
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);


	var cell = row.insertCell(0);
	var element = document.createElement("input");
	element.type = "text";
	element.className += "form-control";
	cell.appendChild(element);
	cell.className += 'inptd';

	cell = row.insertCell(1);
	element = document.createElement("input");
	element.type = "text";
	element.className += "form-control";
	cell.appendChild(element);
	cell.className += 'inptd';

	cell = row.insertCell(2);
	element = document.createElement("input");
	element.type = "text";
	element.className += "form-control";
	cell.appendChild(element);
	cell.className += 'inptd';

	cell = row.insertCell(3);
	element = document.createElement("input");
	element.type = "text";
	element.className += "form-control";
	cell.appendChild(element);
	cell.className += 'inptd';
	
	cell = row.insertCell(4);	
	element = document.createElement("button");
	element.type = "button";
	element.className += "btn btn-primary btn-sm save";
	element.innerHTML = 'Guardar';
	cell.appendChild(element);
	element = document.createElement("button");
	element.type = "button";
	element.className += "btn btn-primary btn-sm edit";
	element.innerHTML = 'Editar';
	cell.appendChild(element);
	element = document.createElement("button");
	element.type = "button";
	element.className += "btn btn-danger btn-sm del";
	element.innerHTML = 'Eliminar';
	cell.appendChild(element);
}